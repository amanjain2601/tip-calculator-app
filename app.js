// Grabbing Buttons and Elements
let billAmount = document.querySelector('.bill');
let percentButtonArray = document.querySelectorAll('.percent-btn');
let customPercentvalue = document.querySelector('.custom');
let noOfpeople = document.querySelector('.people');
let tipAmount = document.querySelector('.tip-amount span');
let totalAmount = document.querySelector('.total-amount span');
let resetButton = document.querySelector('.reset-button');

// Adding Event listeners to Buutons and Elements
billAmount.addEventListener('input', amount);

percentButtonArray.forEach((button) => {
  button.addEventListener('click', percentRead);
});

customPercentvalue.addEventListener('input', customPercentRead);

noOfpeople.addEventListener('input', peopleRead);

resetButton.addEventListener('click', resetAll);

let amountValue,
  percentValue = 0, // Set default value of percentage to 0
  peopleCount;

// Function to fetch amount entered
function amount(event) {
  amountValue = Number(event.target.value);
  event.target.style.border = 'none';
  checkForBorder(event.target);
  display();
}

// Function to fetch percentage from button
function percentRead(event) {
  let buttonSelected = event.target.textContent;

  changeButtonColor(buttonSelected);

  if (
    window.getComputedStyle(event.target).backgroundColor === 'rgb(0, 73, 77)'
  ) {
    event.target.style.backgroundColor = 'rgb(127, 156, 159)';
    percentValue = Number(buttonSelected.slice(0, buttonSelected.length - 1));
  } else {
    event.target.style.backgroundColor = 'rgb(0, 73, 77)';
    percentValue = 0;
  }

  customPercentvalue.value = '';
  display();
}

// Function to fetch custom Percentage entered
function customPercentRead(event) {
  percentValue = Number(event.target.value);
  event.target.style.border = 'none';
  changeButtonColor();
  checkForBorder(event.target);
  display();
}

// Function to fetch no of people entered
function peopleRead(event) {
  peopleCount = Number(event.target.value);
  event.target.style.border = 'none';
  checkForBorder(event.target);
  display();
}

// Function to display result if inputs are valid and sufficient
function display() {
  if (amountValue > 0) {
    if (Number.isInteger(peopleCount) && peopleCount > 0) {
      let tipPerPerson = (
        (amountValue * percentValue) /
        100 /
        peopleCount
      ).toFixed(2);
      tipAmount.textContent = tipPerPerson;

      let totalPerPerson = (
        amountValue / peopleCount +
        Number(tipPerPerson)
      ).toFixed(2);
      totalAmount.textContent = totalPerPerson.toString();
    } else {
      tipAmount.textContent = '0.00';
      totalAmount.textContent = '0.00';
    }
  } else {
    tipAmount.textContent = '0.00';
    totalAmount.textContent = '0.00';
  }
}

// Function to Rese Data if Reset Button was Clicked
function resetAll() {
  billAmount.value = '';
  noOfpeople.value = '';
  customPercentvalue.value = '';
  changeButtonColor();
  tipAmount.textContent = '0.00';
  totalAmount.textContent = '0.00';
  peopleCount = 0;
  percentValue = 0;
  amountValue = 0;
}

// Change background color of percent buttons
function changeButtonColor(buttonSelected) {
  percentButtonArray.forEach((button) => {
    if (button.textContent !== buttonSelected)
      button.style.backgroundColor = 'rgb(0, 73, 77)';
  });
}

// Change border of input field
function checkForBorder(targetElement) {
  if (targetElement.value === '') {
    targetElement.style.border = '2px solid black';
  }
}
